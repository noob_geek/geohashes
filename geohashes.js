var Geohash = require('latlon-geohash');
var randomstring= require('randomstring');
const Trie = require("prefix-trie-ts").Trie;

//bangalore's geohash
var bglore=Geohash.encode(12.9716,77.5946,4);

var bglore_neigh=Geohash.neighbours(bglore);
var bounds=Geohash.bounds(bglore);

var x=10;

var customer_locations= [];
//generate random geohashes for the customer locations
for(var i=0;i<5000;i++){
	customer_locations.push(bglore+randomstring.generate(3));
}

//trie for storing locations
var trie = new Trie(customer_locations);

// get number of cutomers in the areas.

console.log("Area-Geohashes,"+"Number Of Customers");

for(var i=0;i<10;i++){
	console.log(bglore+i,trie.getPrefix(bglore+i).length);	
}
for(var i=0;i<26;i++){
	var alphabet = String.fromCharCode(65 + i);
	console.log(bglore+alphabet,trie.getPrefix(bglore+alphabet).length)
}
for(var i=0;i<26;i++){
	var alphabet = String.fromCharCode(97 + i);
	console.log(bglore+alphabet,trie.getPrefix(bglore+alphabet).length)
}
